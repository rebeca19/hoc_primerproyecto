# HOC_PrimerProyecto

Seminario de Heurísticas y Optimización Combinatoria 2021-1\
Proyecto 1\
:earth_americas: Traveling Salesman Problem con Recocido Simulado :earth_asia:

## Autora
Alma Rosa Páes Alcalá :beetle:

---

## Versiones de software
* **rustc** `1.47.0`
* **cargo** `1.47.0`
* Versiones de dependencias utilizadas se encuentran en el archivo [**Cargo.toml**](Cargo.toml)

# Requerimientos

La base de datos debe estar creada en SQLite, y rellenada con el script proporcionado en [tsp.sql](tsp.sql).


## Modo de uso
**Para compilar:** `cargo build`\
**Para ejecutar las pruebas:** `cargo test`\
**Para generar y ver la documentacion:** `cargo doc --open --no-deps` \
**Para ver la documentación:** Abrir en navegador el [documento principal](doc/tsp_project/index.html). \
**Para correr el programa:** 
`cat {instancia_file} | cargo run {semillas}` o \
`cargo run {semillas} < {instancia_file}`

## Mejores semillas
Cada instancia proporcionada fue probada con alrededor de 1000 semillas, que corresponden a los primeros 1000 números naturales.

Para la [instancia de 40 ciudades](instancias/instancia1.txt):
* Semilla **755**, cuya solución tiene un costo de `0.224633`
* Semilla **449**, cuya solución tiene un costo de `0.227558`
* Semilla **228**, cuya solución tiene un costo de `0.230544`

Para la [instancia de 150 ciudades](instancias/instancia2.txt):
* Semilla **3**, cuya solución tiene un costo de `0.166028`
* Semilla **433**, cuya solución tiene un costo de `0.168154`
* Semilla **329**, cuya solución tiene un costo de `0.172286`
