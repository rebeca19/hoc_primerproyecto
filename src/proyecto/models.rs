/// Estructura que modela a las ciudades de la gráfica
#[derive(Queryable,Clone)]
pub struct Ciudad {
    pub id: i32,
    pub nombre: String,
    pub pais: String,
    pub poblacion: i32,
    pub latitud: f64,
    pub longitud: f64
}

/// Estructura que modela a las conexiones entre ciudades
#[derive(Queryable,Clone)]
pub struct Conexion{
    pub id_ciudad_1: i32,
    pub id_ciudad_2: i32,
    pub distancia: f64
}