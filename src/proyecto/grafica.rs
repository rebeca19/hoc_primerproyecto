use crate::proyecto::models::Ciudad;
use crate::proyecto::models::Conexion;

/// Modelo de la gráfica original, con las ciudades y conexiones entre ellas
pub struct Grafica{
    /// Vector de ciudades presentes en la gráfica
    ciudades: Vec<Ciudad>,

    /// Matriz de conexiones entre ciudades
    conexiones: Vec<Vec<f64>>,
}

/// Crea la matriz de conexiones entre ciudades de la gráfica.
/// Se busca optimizar el tiempo de búsqueda de la distancia entre dos ciudades
/// 
/// # Arguments
/// * `conects` - vector de las conexiones recopiladas en la base de datos
/// * `longitud` - cantidad de ciudades que podrían estar conectadas
/// 
/// # Returns
/// * `Vec<Vec<f64>>` - Matriz con las conexiones entre ciudades
fn create_matrix(conects: Vec<Conexion>, longitud: usize) -> Vec<Vec<f64>> {
    let mut matriz: Vec<Vec<f64>> = vec![vec![0.0f64; longitud]; longitud];
    for ruta in conects.into_iter(){
        matriz[ruta.id_ciudad_1 as usize][ruta.id_ciudad_2 as usize] = ruta.distancia;
    }
    matriz
}

impl Grafica{
    /// Crea una nueva gráfica
    /// 
    /// # Arguments
    /// * `cities` - ciudades que son parte de la gráfica
    /// * `conect` - lista de conexiones entre las ciudades
    pub fn new(cities: Vec<Ciudad>, conect: Vec<Conexion>) -> Grafica{
        let temp = &cities;
        Grafica {
            conexiones: create_matrix(conect, temp.len()+1 as usize),
            ciudades: cities,
        }
    }

    /// Obtiene la distancia entre dos ciudades
    /// 
    /// # Arguments
    /// * `c1` - id de la primera ciudad
    /// * `c2` - id de la segunda ciudad
    /// 
    /// # Returns
    /// * `f64` - la distancia entre las dos ciudades
    pub fn get_distancia(&self, c1: i32, c2: i32) -> f64{
        let mayor;
        let menor = 
            if c1 < c2{
                mayor = c2;
                c1
            } else {
                mayor = c1;
                c2
            };
        self.conexiones[menor as usize][mayor as usize]
    }

    /// Obtiene la latitud y la longitud de una ciudad en **grados**
    /// 
    /// # Arguments
    /// * `id_city` - id de la ciudad en la gráfica
    /// 
    /// # Returns
    /// * `(f64,f64)` - tupla con la latitud y la longitud de la ciudad, respectivamente.
    pub fn get_coordenadas_ciudad(&self, id_city: i32) -> (f64,f64){
        (self.ciudades[(id_city-1) as usize].latitud, self.ciudades[(id_city-1) as usize].longitud)
    }
}